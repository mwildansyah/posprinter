package com.example.printerbluetoothtest;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class AdapterBluetooth extends RecyclerView.Adapter<AdapterBluetooth.ViewHolder> {

    private List<BluetoothDevice> bluetoothDevice = new ArrayList<>();
    private onClick onClick;
    private Activity activity;

    public AdapterBluetooth.onClick getOnClick() {
        return onClick;
    }

    public void setOnClick(AdapterBluetooth.onClick onClick) {
        this.onClick = onClick;
    }

    public AdapterBluetooth(List<BluetoothDevice> bluetoothDevice, Activity activity) {
        this.bluetoothDevice = bluetoothDevice;
        this.activity = activity;
    }

    public interface onClick{
        void onClickDevice(BluetoothDevice device);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bluetooth_device, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BluetoothDevice device = bluetoothDevice.get(position);
        if (device.getName() != null){
            if (!device.getName().isEmpty() || !device.getName().equals("")){
                Log.d("TAG DEVICE NAME", "DEVICE NAME "+device.getName());
                holder.tvName.setText(device.getName());
//        if (device.getBluetoothClass().getDeviceClass() == IMAGING){
                holder.ivImage.setImageResource(R.drawable.ic_printer);
//        }
                holder.lnDevice.setOnClickListener(v -> onClick.onClickDevice(device));
                holder.lnDevice.setVisibility(View.VISIBLE);
            }else {
                holder.lnDevice.setVisibility(View.GONE);
            }
        }else {
            holder.lnDevice.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return bluetoothDevice.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tvName;
        private LinearLayout lnDevice;
        private ImageView ivImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            ivImage = itemView.findViewById(R.id.iv_image);
            lnDevice = itemView.findViewById(R.id.ln_device);
        }
    }
}
