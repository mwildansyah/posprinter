//package com.example.printerbluetoothtest;
//
//import androidx.annotation.Nullable;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.core.app.ActivityCompat;
//import androidx.core.content.ContextCompat;
//import androidx.recyclerview.widget.RecyclerView;
//
//import android.bluetooth.BluetoothAdapter;
//import android.bluetooth.BluetoothDevice;
//import android.bluetooth.BluetoothSocket;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.Button;
//import android.widget.Toast;
//
//import com.dantsu.escposprinter.EscPosPrinter;
//import com.dantsu.escposprinter.connection.bluetooth.BluetoothConnection;
//import com.dantsu.escposprinter.exceptions.EscPosBarcodeException;
//import com.dantsu.escposprinter.exceptions.EscPosConnectionException;
//import com.dantsu.escposprinter.exceptions.EscPosEncodingException;
//import com.dantsu.escposprinter.exceptions.EscPosParserException;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.UUID;
//
//import static android.Manifest.permission.BLUETOOTH;
//import static com.example.printerbluetoothtest.NewPrinterAtoz.EXTRAS_BT_DEVICE;
//import static com.example.printerbluetoothtest.PrinterCommandAtoz.ALIGN_TEXT_CENTER;
//import static com.example.printerbluetoothtest.PrinterCommandAtoz.OPEN_PRINTER_ATOZ_CODE;
//import static com.example.printerbluetoothtest.PrinterCommandAtoz.REQUEST_CODE_BT;
//import static com.example.printerbluetoothtest.PrinterCommandAtoz.REQUEST_ENABLE_BT;
//import static com.example.printerbluetoothtest.PrinterCommandAtoz.SIZE_TEXT_NORMAL;
//
//public class MainActivity extends AppCompatActivity {
//    private EscPosPrinter escPosPrinter;
//    private BluetoothConnection bluetoothConnection;
//    private BluetoothSocket bluetoothSocket;
//    private BluetoothDevice bluetoothDevice;
//    private Runnable succesConnectRunnable = () -> Toast.makeText(getApplicationContext(), "Device Connected", Toast.LENGTH_SHORT).show();
//    private Runnable socketErrorRunnable = () -> Toast.makeText(getApplicationContext(), "Cannot connect to device", Toast.LENGTH_SHORT).show();
//
//    private PrinterConnectionAtoz printerConnectionAtoz;
//    private PrinterCommandAtoz printerCommandAtoz;
//    private PrinterCommandAtoz.PrinterAtoz printerAtoz;
//
//    private Button btnFind, btnPrint;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.act_main);
//
//        btnFind = findViewById(R.id.find);
//        btnPrint = findViewById(R.id.print);
//
//        printerConnectionAtoz = PrinterConnectionAtoz.getInstance(this);
//        printerCommandAtoz = PrinterCommandAtoz.getInstance(this);
//
//        btnFind.setOnClickListener(v -> findDevice());
//
//        btnPrint.setOnClickListener(v -> print());
//
//    }
//
//    private void findDevice() {
//        if (ContextCompat.checkSelfPermission(this, BLUETOOTH) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this, new String[]{BLUETOOTH}, REQUEST_CODE_BT);
//            return;
//        }
//        printerCommandAtoz.openBluetoothDevice();
//    }
//
//    private void print() {
//        if (printerAtoz == null) {
//            Toast.makeText(this, "Connect Bluetooth first", Toast.LENGTH_SHORT).show();
//            return;
//        }
//
//        List<FormatTextAtoz> format = new ArrayList<>();
//        FormatTextAtoz ftText = new FormatTextAtoz();
//        ftText.setText("TEXT");
//        ftText.setAlign(ALIGN_TEXT_CENTER);
//        ftText.setSize(SIZE_TEXT_NORMAL);
//        format.add(ftText);
//        format.add(ftText);
//        format.add(ftText);
//        String text = printerAtoz.createText(format);
//        printerAtoz.printText(text);
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == OPEN_PRINTER_ATOZ_CODE) {
//            if (data != null) {
//                if (data.getParcelableExtra(EXTRAS_BT_DEVICE) != null) {
//                    bluetoothDevice = data.getParcelableExtra(EXTRAS_BT_DEVICE);
//                    escPosPrinter = printerConnectionAtoz.createConnection(bluetoothDevice);
//                    printerAtoz = new PrinterCommandAtoz.PrinterAtoz(escPosPrinter);
//                }
//            }
//        }
//    }
//}