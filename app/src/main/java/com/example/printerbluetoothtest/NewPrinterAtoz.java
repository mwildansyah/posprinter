package com.example.printerbluetoothtest;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dantsu.escposprinter.EscPosPrinter;
import com.dantsu.escposprinter.connection.bluetooth.BluetoothConnection;
import com.dantsu.escposprinter.exceptions.EscPosConnectionException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class NewPrinterAtoz extends AppCompatActivity {

    private Toolbar toolbar;

    private BluetoothAdapter bluetoothAdapter;
    private List<BluetoothDevice> listDevice = new ArrayList<>();
    private AdapterBluetooth adapterBluetooth;
    private RecyclerView rvBluetoothDevice;
    private BluetoothSocket bluetoothSocket;
    private BluetoothDevice bluetoothDevice;

    private ProgressBar progressBar;

    private ImageView btnBack;
    private static final UUID SPP_UUID = UUID
            .fromString("00001101-0000-1000-8000-00805F9B34FB");
    public static final String EXTRAS_BT_DEVICE = "EXTRAS_BT_DEVICE";

    private Runnable socketErrorRunnable = () -> Toast.makeText(getApplicationContext(), "Cannot connect to device", Toast.LENGTH_SHORT).show();

    public BluetoothDevice getBluetoothDevice() {
        return bluetoothDevice;
    }

    public void setBluetoothDevice(BluetoothDevice bluetoothDevice) {
        this.bluetoothDevice = bluetoothDevice;
    }

    public BluetoothSocket getBluetoothSocket() {
        return bluetoothSocket;
    }

    public void setBluetoothSocket(BluetoothSocket bluetoothSocket) {
        this.bluetoothSocket = bluetoothSocket;
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void setToolbar(Toolbar toolbar) {
        this.toolbar = toolbar;
    }

    @Override
    public void onBackPressed() {
        passingBtDevice(bluetoothDevice);
        finish();
    }

    private void passingBtDevice(BluetoothDevice bluetoothDevice) {
        setResult(RESULT_OK, getIntent().putExtra(EXTRAS_BT_DEVICE, bluetoothDevice));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.device_list);

        setToolbar((Toolbar) findViewById(R.id.toolbar_bluetooth));
        setSupportActionBar(getToolbar());

        btnBack = findViewById(R.id.back_button);
        btnBack.setOnClickListener(v -> {
            onBackPressed();
        });

        adapterBluetooth = new AdapterBluetooth(listDevice, this);
        rvBluetoothDevice = findViewById(R.id.rv_bluetooth);
        rvBluetoothDevice.setLayoutManager(new LinearLayoutManager(this));
        rvBluetoothDevice.setAdapter(adapterBluetooth);
        adapterBluetooth.setOnClick(device -> {
            Toast.makeText(this, "Connecting devices.. ", Toast.LENGTH_SHORT).show();
            try {
                deviceConectting(device);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.cancelDiscovery();
        }

        progressBar = findViewById(R.id.progress);
        if (listDevice.size() != 0){
            progressBar.setVisibility(View.GONE);
        }

//        EscPosPrinter printer = PrinterConnectionAtoz.getInstance(this).createConnection(bluetoothDevice);
    }

    protected void proceedDiscovery() {
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothDevice.ACTION_NAME_CHANGED);
        registerReceiver(mBTReceiver, filter);

        bluetoothAdapter.startDiscovery();
    }

    public void deviceConectting(final BluetoothDevice bluetoothDevice) throws IOException {

        Thread connectThread = new Thread(() -> {
            try {
                if (bluetoothDevice.getBondState() == 10){
                    setBluetoothDevice(bluetoothDevice);
                    setBluetoothSocket(bluetoothDevice.createRfcommSocketToServiceRecord(SPP_UUID));
                    getBluetoothSocket().connect();
                    setBluetoothDevice(bluetoothDevice);
                }
                setResult(RESULT_OK, getIntent().putExtra(EXTRAS_BT_DEVICE, bluetoothDevice));
                finish();
            } catch (IOException ex) {
                runOnUiThread(socketErrorRunnable);
                ex.printStackTrace();
                try {
                    if (getBluetoothSocket() != null){
                        getBluetoothSocket().close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (getBluetoothSocket() != null){
                    setBluetoothSocket(null);
                }
            }
        });

        connectThread.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        proceedDiscovery();
    }

    private final BroadcastReceiver mBTReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent
                        .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                boolean same = false;
                if (listDevice.size() != 0){
                    for (int i = 0;i < listDevice.size(); i++){
                        if (listDevice.get(i).getAddress().equals(device.getAddress())){
                            same = true;
                        }
                    }
                    if (!same){
                        listDevice.add(device);
                    }
                }else {
                    listDevice.add(device);
                }
//                bluetoothAdapter.cancelDiscovery();
                adapterBluetooth.notifyDataSetChanged();
            }
            if (listDevice.size() != 0){
                progressBar.setVisibility(View.GONE);
            }
        }
    };

}
