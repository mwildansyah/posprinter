package com.example.printerbluetoothtest;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.dantsu.escposprinter.EscPosPrinter;
import com.dantsu.escposprinter.exceptions.EscPosBarcodeException;
import com.dantsu.escposprinter.exceptions.EscPosConnectionException;
import com.dantsu.escposprinter.exceptions.EscPosEncodingException;
import com.dantsu.escposprinter.exceptions.EscPosParserException;

import java.util.ArrayList;
import java.util.List;

public class PrinterCommandAtoz {

    private Activity activity;
    private static  PrinterCommandAtoz mInstance = null;
    public static final int OPEN_PRINTER_ATOZ_CODE = 2804;
    public static final int REQUEST_ENABLE_BT = 887;
    public static final int REQUEST_CODE_BT = 878;
    private BluetoothAdapter bluetoothAdapter;

    public static final String SIZE_TEXT_NORMAL = "<font size='normal'>";
    public static final String SIZE_TEXT_BIG = "<font size='big'>";

    public static final String ALIGN_TEXT_LEFT = "[L]";
    public static final String ALIGN_TEXT_RIGHT = "[R]";
    public static final String ALIGN_TEXT_CENTER = "[C]";


    public PrinterCommandAtoz(Activity activity) {
        this.activity = activity;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    @NonNull
    public static PrinterCommandAtoz getInstance(Activity activity){
        if (mInstance == null){
            mInstance = new PrinterCommandAtoz(activity);
        }
        return mInstance;
    }



    public void openBluetoothDevice(){
        if (bluetoothAdapter == null){
            Toast.makeText(activity, "Device doesn't support bluetooth", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }else {
            Intent intent = new Intent(activity, NewPrinterAtoz.class);
            activity.startActivityForResult(intent, OPEN_PRINTER_ATOZ_CODE);
        }
    }

    public void openBluetoothDevice(int requestCode){
        if (bluetoothAdapter == null){
            Toast.makeText(activity, "Device doesn't support bluetooth", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }else {
            Intent intent = new Intent(activity, NewPrinterAtoz.class);
            activity.startActivityForResult(intent, requestCode);
        }
    }

    public static class PrinterAtoz{

        private static PrinterAtoz printerInstance = null;
        private EscPosPrinter printer = null;

        public PrinterAtoz(EscPosPrinter printer) {
            this.printer = printer;
        }

        public String getSize(String size){
            String fontSize = "<font size='normal'>";
            switch (size){
                case "normal":
                    fontSize = "<font size='normal'>";
                    break;
                case "big":
                    fontSize = "<font size='big'>";
                    break;
            }
            return fontSize;
        }

        public void printText(String text){
            try {
                printer.printFormattedText(
                        text
                );
            } catch (EscPosConnectionException exception) {
                exception.printStackTrace();
            } catch (EscPosParserException e) {
                e.printStackTrace();
            } catch (EscPosEncodingException e) {
                e.printStackTrace();
            } catch (EscPosBarcodeException e) {
                e.printStackTrace();
            }
        }

        public String createText(List<FormatTextAtoz> formatTextAtozs){

            StringBuilder text = new StringBuilder();
            for (FormatTextAtoz format : formatTextAtozs){
                if (format.getStyle() != null){
                    if (!format.getStyle().equals("")){
                        text.append(format.getFormatStyle());
                    }else {
                        text.append(format.getFormatText());
                    }
                }else {
                    text.append(format.getFormatText());
                }
            }

            return text.toString();
        }






    }

}
