package com.example.printerbluetoothtest;

public class FormatTextAtoz {

    String text;
    String align;
    String size;
    String style;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAlign() {
        return align;
    }

    public void setAlign(String align) {
        this.align = align;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getFormatText(){
        return getAlign()+getSize()+getText()+"</font>\n";
    }

    public String getFormatStyle(){
        return getAlign()+"<"+getStyle()+">"+getSize()+getText()+"</font>"+"</"+getStyle()+">\n";
    }

}
