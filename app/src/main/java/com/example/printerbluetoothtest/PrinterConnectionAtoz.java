package com.example.printerbluetoothtest;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dantsu.escposprinter.EscPosPrinter;
import com.dantsu.escposprinter.connection.bluetooth.BluetoothConnection;
import com.dantsu.escposprinter.exceptions.EscPosConnectionException;

public class PrinterConnectionAtoz {

    private static EscPosPrinter escPosPrinter = null;
    private static BluetoothConnection bluetoothConnection = null;
    private static PrinterConnectionAtoz mInstance = null;
    private Activity activity;

    private Runnable succesConnectRunnable = () -> Toast.makeText(activity, "Device Connected", Toast.LENGTH_SHORT).show();
    private Runnable socketErrorRunnable = () -> Toast.makeText(activity, "Cannot connect to device", Toast.LENGTH_SHORT).show();

    public PrinterConnectionAtoz(Activity activity) {
        this.activity = activity;
    }

    @NonNull
    public static PrinterConnectionAtoz getInstance(Activity activity){
        if (mInstance == null){
            mInstance = new PrinterConnectionAtoz(activity);
        }
        return mInstance;
    }

    public EscPosPrinter createConnection(BluetoothDevice device){
        if (bluetoothConnection != null){
            if (bluetoothConnection.isConnected()) {
                bluetoothConnection.disconnect();
            }
        }
        bluetoothConnection = new BluetoothConnection(device);
        return this.connectPrinter(bluetoothConnection);
    }

    private EscPosPrinter connectPrinter(BluetoothConnection bluetoothConnection){
        try {
            escPosPrinter = new EscPosPrinter(bluetoothConnection, 203, 48f, 32);
            activity.runOnUiThread(succesConnectRunnable);
        } catch (EscPosConnectionException e) {
            activity.runOnUiThread(socketErrorRunnable);
            e.printStackTrace();
        }
        return escPosPrinter;
    }

}
